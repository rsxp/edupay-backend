# Backend to supply data to app for parents and children

## Steps to run project

In the project directory, you can run:

### `yarn` or `npm install`

This will add the dependencies required to run the project

### `yarn start` or `npm start`

This will start the project

## Routes
| Endpoint                                     | Description                                                                                                |
| -------------------------------------------- | ---------------------------------------------------------------------------------------------------------- |
| GET `/users` **PUBLIC**                 | Get All users.                             |
| GET `/questions` **PUBLIC**        | Get all Questions **Required Query**: subject: string |
| POST `/answer` **PUBLIC** | Answer a question **Required**: subject: string, stars: number                                     |
| GET `/purchases` **PUBLIC**              | Get all purchases from a son. **Required Query**: userId: String                 |
| GET `/progress`**PUBLIC**               | Get the progress from a son. **Required query**: userId: String                               |
