export const users = [
  {
    id: 1,
    name: 'Carlos Silva',
    username: 'carlossilva',
    type: 'parent',
    sonId: 2,
    parentId: null,
    amount: 300,
  },
  {
    id: 2,
    name: 'Carlos Junior',
    username: 'carlosjunior',
    type: 'son',
    parentId: 1,
    amount: 0,
  },
];

class UserController {
  index(req, res) {
    res.json(users);
  }
}

export default new UserController();
