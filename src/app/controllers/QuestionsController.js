export const questions = [
  {
    subject: 'geografia',
    value: 7,
    description:
      'No exemplo abaixo mostra uma lista dos estados que compõem a região sudeste do Brasil, selecione qual estado que está faltando adicionar nessa lista:',
    descriptionCode:
      'const estados = [‘São Paulo’, ‘Rio de Janeiro’, ‘Minas Gerais’]; estados.push(???);',
    options: [
      {
        id: 1,
        answer: 'Mato Grosso',
      },
      {
        id: 2,
        answer: 'Paraná',
      },
      {
        id: 3,
        answer: 'Espírito Santo',
      },
      {
        id: 4,
        answer: 'Distrito Federal',
      },
    ],
  },
  {
    subject: 'historia',
    value: 7,
    description:
      'No exemplo abaixo mostra uma lista de presidentes do Brasil, remova o presidente que não é brasileiro',
    descriptionCode:
      'const presidentes = [‘Luiz Inácio Lula da Silva’, ‘Hugo Chaves’, ‘Jair Bolsonaro’, ‘Deodoro da Fonseca’]; presidentes.filter(presidente => presidente.toLowerCase() !== (?) );',
    options: [
      {
        id: 1,
        answer: 'Hugo Chaves',
      },
      {
        id: 2,
        answer: 'Jair Bolsonaro',
      },
      {
        id: 3,
        answer: 'Luiz Inácio Lula da Silva',
      },
      {
        id: 4,
        answer: 'Deodoro da Fonseca',
      },
    ],
  },
  {
    subject: 'biologia',
    value: 7,
    description:
      'No exemplo abaixo mostra uma lista de animais, remova o animal que não é mamífero',
    descriptionCode:
      'const mamiferos = [‘Leão’, ‘Urso’, ‘cachorro’]; mamiferos.filter(mamifero => mamifero !== (?) );',
    options: [
      {
        id: 1,
        answer: 'Leão',
      },
      {
        id: 2,
        answer: 'Urso',
      },
      {
        id: 3,
        answer: 'Cachorro',
      },
      {
        id: 4,
        answer: 'urso',
      },
    ],
  },
];

class QuestionsController {
  show(req, res) {
    const { subject } = req.query;

    const question = questions.find(q => q.subject === subject);

    if (!question) {
      return res.status(400).json({ error: 'Question Not Found' });
    }

    return res.json(question);
  }
}

export default new QuestionsController();
