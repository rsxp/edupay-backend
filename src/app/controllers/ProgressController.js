import { users } from './UserController';

const progresses = [
  {
    userId: 2,
    progress: [
      {
        id: 1,
        subject: 'geografia',
        stars: 3,
        amount: 7,
      },
      {
        id: 2,
        subject: 'historia',
        stars: 2,
        amount: 5,
      },
      {
        id: 1,
        subject: 'biologia',
        stars: 1,
        amount: 3,
      },
    ],
  },
];

class ProgressController {
  show(req, res) {
    const user = users.find(u => u.id === Number(req.query.userId));

    const { progress } = progresses.find(
      p => p.userId === user.sonId || p.userId === user.id
    );

    return res.json(progress);
  }
}

export default new ProgressController();
